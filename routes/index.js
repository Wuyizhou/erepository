var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var User = require('../models/user');
var Post = require('../models/post');

router.get('/', function(req, res) {
  Post.getAll(null,function(err, posts){
  	if(err){
  		posts = [];
  	};
  res.render('index', { 
  	title: 'e',
  	user: req.session.user,
  	success: req.flash('success'),
  	error: req.flash('error'),
  	posts: posts
  });
 });
});


router.get('/search', function(req,res){
	Post.search(req.query.keyword, function (err, posts) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    res.render('search', {
      title: 'e',
      posts: posts,
      user: req.session.user
    });
  });
});

router.get('/u/:name/:day/:title', function(req,res){
  Post.getOne(req.params.name, req.params.day, req.params.title, function (err, post) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    res.render('article', {
      title: 'e',
      post: post,
      user: req.session.user,
    });
  });
});

router.get('/edit/:name/:day/:title', checkLogin);
router.get('/edit/:name/:day/:title', function(req,res){
	Post.edit(req.params.name, req.params.day, req.params.title, function(err,post){
		if(err){
			req.flash('error', err);
			return res.redirect('/');
		};
		res.render('edit', {
			title: 'e',
			post: post,
			user: req.session.user
		});
	});
});

router.post('/edit/:name/:day/:title', checkLogin);
router.post('/edit/:name/:day/:title', function (req, res) {
  var currentUser = req.session.user;
  Post.update(currentUser.username, req.params.day, req.params.title, req.body.post, function (err) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    req.flash('success', '修改成功!');
    res.redirect('/');
  });
});

router.get('/remove/:name/:day/:title', checkLogin);
router.get('/remove/:name/:day/:title', function (req, res) {
  var currentUser = req.session.user;
  Post.remove(currentUser.username, req.params.day, req.params.title, function (err) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('back');
    }
    req.flash('success', '删除成功!');
    res.redirect('/');
  });
});

router.get('/login', checkNotLogin);
router.get('/login', function(req,res){
  res.render('login', { 
  	title: 'e',
  	user: req.session.user
  });
});

router.post('/login', checkNotLogin);
router.post('/login', function(req,res){
	var username = req.body.username,
		password = req.body.password;
		username = username.replace(/(\!+)|(\<+)|(\>+)|(\/+)|(\\+)|(\"+)|(\'+)/g,"");
		password = password.replace(/(\!+)|(\<+)|(\>+)|(\/+)|(\\+)|(\"+)|(\'+)/g,"");
		console.log(username);
		console.log(password);

	var md5 = crypto.createHash('md5'),
		password = md5.update(req.body.password).digest('hex');

	User.get(req.body.username, function(err, user){
		if(err){
			req.flash('error', 'user error');
			return res.redirect('/login');
		};
		if(user.password != password){
			req.flash('error', 'password error');
			return res.redirect('/login');
		};

		req.session.user = user;
		req.flash('success', 'login ok!');
		res.redirect('/');
	});
});

/**  stop reg
router.get('/reg', function(req,res){
	req.flash('error', 'Stop registration, please contact the administrator');
	res.redirect('/');
})
**/
router.get('/reg', checkNotLogin);
router.get('/reg', function(req,res){
  res.render('reg', { 
  	title: 'e',
  	user: req.session.user
  });
});

router.post('/reg', checkNotLogin);
router.post('/reg', function(req,res){
	var username = req.body.username,
		password = req.body.password,
		password_re = req.body['password-repeat'];
	if(password != password_re){
		req.flash('error', 'password repeat error');
		return res.redirect('/reg');
	};
	var md5 = crypto.createHash('md5'),
		password = md5.update(req.body.password).digest('hex');
	var newUser = new User({
		username: username,
		password: password,
		email: req.body.email
	});
	User.get(newUser.username, function(err,user){
		if(user){
			req.flash('error', 'User already exists');
			return res.redirect('/reg');
		};
		newUser.save(function(err,user){
			if(err){
				req.flash('error',err);
				return res.redirect('/reg');
			};
			req.session.user = user;
			req.flash('success', 'reg ok!');
			res.redirect('/');
		});
	});
});

router.get('/post', checkLogin);
router.get('/post', function(req,res){
	res.render('post', { 
  	title: 'e',
  	user: req.session.user,
  	success: req.flash('success'),
  	error: req.flash('error')
  	});
});

router.post('/post', checkLogin);
router.post('/post', function(req,res){
	var currentUser = req.session.user,
		post = new Post(currentUser.username, req.body.title, req.body.post);
		console.log(post);
		post.save(function(err){
			if(err){
				req.flash('error',err);
				return res.redirect('/');
			};
			req.flash('success', 'post ok!');
			res.redirect('/');
		})
})


router.get('/loginout', checkLogin);
router.get('/loginout', function(req,res){
	req.flash('success', 'loginout ok!');
	req.session.user = null;
	res.redirect('/');
});

router.get('/about', function(req,res){
  res.render('about', { 
  	title: 'e',
  	user: req.session.user
  });
});

function checkLogin(req,res,next){
	if(!req.session.user){
		req.flash('error', 'not login');
		res.redirect('/');
	};
	next();
};

function checkNotLogin(req,res,next){
	if(req.session.user){
		req.flash('error', 'login ing..');
		res.redirect('/');
	};
	next();
};

module.exports = router;
